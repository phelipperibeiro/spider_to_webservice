<?php

include(dirname(__FILE__) . '/SintegraSpider.php');

include_once('Zend/Soap/Server.php');
include_once('Zend/Soap/AutoDiscover.php');

if (isset($_GET['wsdl'])) {
    /*
     * Usar o Soap AutoDiscover para criacao do WSDL de forma dinamica          
     */
    $autodiscover = new Zend_Soap_AutoDiscover();
    $autodiscover->setClass('SintegraService');
    $autodiscover->handle();
} else {
    // Disponibilizar o webservice atraves do canal: 
    $soap = new Zend_Soap_Server("http://localhost/spider_to_webservice/SintegraService.php?wsdl");
    $soap->setClass('SintegraService');
    $soap->handle();
}

class SintegraService {

    /**
     * Retorna todos os dados do CNPJ ou INSCRIÇÃO ESTADUAL.
     *
     * @param string $num_cnpj
     * @param string $num_ie
     * @return array $campos
     */
    function consultaSituacao($num_cnpj, $num_ie) {
        try {
            $spider =  new SintegraSpider();
            return $spider->consultaSituacao($num_cnpj, $num_ie);
        } catch (Exception $e) {
            throw $e->getMessage();
        }
    }

}

