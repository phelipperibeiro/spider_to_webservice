<?php
include(dirname(__FILE__) . '/Model.php');

class SintegraModel extends Model
{

    private $conexao;
    public $campos;
 

    public function __construct()
    {
        $this->conexao = null;
        $this->campos = array();
       
    }

    private function model($sql, $parametro = null, $tabela = null, $tipoAcesso = null)
    {
        $this->conexao = empty($this->conexao) ? parent::getConexao() : $this->conexao;
        $sth = $this->conexao->prepare($sql);
        $parametro = !empty($parametro) ? $parametro : null;
        if ($sth->execute($parametro)) {
            if (!empty($tipoAcesso) && $tipoAcesso == 'select') {
                return $sth->fetchAll(\PDO::FETCH_OBJ);
            }
            if ($sth->rowCount() == 0) {
                echo $tabela;
                echo '<br>';
                var_dump($this->conexao->errorInfo());
                echo '<br>';
                var_dump($this->conexao->errorCode());
                echo '<br>';
                $sth->debugDumpParams();
                echo '<br>';
                echo '<pre>' . print_r($parametro) . '</pre>';
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function insertDados()
    {
        $parametro = array();
        
        foreach ($this->campos as $campo => $value) {
            $parametro[':'.strtoupper($campo)] = $this->campos[$campo];
        }

        $sql = "INSERT INTO CONSULTAS (CNPJ,
                                       INSCRICAO_ESTADUAL,
                                       RAZAO_SOCIAL,
                                       LOGRADOURO,
                                       NUMERO,
                                       COMPLEMENTO,
                                       BAIRRO,
                                       MUNICIPIO,
                                       CEP,
                                       UF,
                                       TELEFONE,
                                       ATIVIDADE,
                                       DATA_INICIO,
                                       STATUS,
                                       DATA_STATUS,
                                       REGIME) VALUES (:CNPJ,
                                                       :INSCRICAO_ESTADUAL,
                                                       :RAZAO_SOCIAL,
                                                       :LOGRADOURO,
                                                       :NUMERO,
                                                       :COMPLEMENTO,
                                                       :BAIRRO,
                                                       :MUNICIPIO,
                                                       :CEP,
                                                       :UF,
                                                       :TELEFONE,
                                                       :ATIVIDADE,
                                                       :DATA_INICIO,
                                                       :STATUS,
                                                       :DATA_STATUS,
                                                       :REGIME)";

        if (!$this->model($sql, $parametro, 'CONSULTAS', 'INSERT')) {
            return false;
        }
        $this->fechaConexao();
        return true;
    }
    
    private function fechaConexao(){
        unset($this->conexao);
    }

}
