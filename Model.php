<?php
include(dirname(__FILE__) . '/Conexao.php');

class Model extends Conexao
{

    const PATH = "config/config.ini";

    protected static function getConexao()
    {
        parent::openconfig(self::PATH);
        return parent::conectar();
    }

}
