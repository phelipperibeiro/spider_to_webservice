<?php

include(dirname(__FILE__) . '/SintegraSpider.php');

//01.624.099/0001-65
//28.152.650/0001-71
// usar como test
try {

    $num_cnpj = isset($_GET['num_cnpj']) ? $_GET['num_cnpj'] : '01.624.099/0001-65';
    $num_ie = isset($_GET['num_ie']) ? $_GET['num_ie'] : '';

    $spider = new SintegraSpider();
    $consulta = $spider->consultaSituacao($num_cnpj, $num_ie);
    echo '<pre>';
    print_r($consulta);
    exit;
} catch (Exception $e) {

    echo $e->getMessage() . PHP_EOL;
}
