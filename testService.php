<?php

$pathToZend = dirname(__FILE__) . '/../ZendFramework-1.10.8/library/';
set_include_path(get_include_path() . PATH_SEPARATOR . $pathToZend);

include_once('Zend/Soap/Client.php');

try {
   
   $num_cnpj = isset($_GET['num_cnpj']) ? $_GET['num_cnpj'] : '';
   $num_ie = isset($_GET['num_ie']) ? $_GET['num_ie'] : '080.250.16-5';
   
  
   $client = new Zend_Soap_Client("http://localhost/spider_to_webservice/SintegraService.php?wsdl");
   
   $consulta = $client->consultaSituacao($num_cnpj, $num_ie); 
} catch (Exception $e) {
   
   echo $e->getMessage() . "\n";
   
}

echo '<pre>', print_r($consulta);